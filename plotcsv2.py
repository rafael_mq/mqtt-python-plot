import csv
import time
from matplotlib import pyplot as plt

file_name = [
        'pwmout_24100927.csv',
        'pwmout-30100927.csv',
        'pwmout-02110927.csv']


# Primeiro dia: start=23 às 0:0:0, end=24 às 0:0:0
# Segundo dia:  start=28 às 23:0:0, end=29 às 23:0:0
def value_index(timestamp, day, hour):
    for i in timestamp:
        if i.tm_mday == day and \
           i.tm_hour == hour and \
           i.tm_min == 18 and i.tm_sec < 30:
            return timestamp.index(i)


values = []
timestamp = []
timeInHours = []
with open(file_name[2]) as csv_file:
    csv_read = csv.DictReader(csv_file, delimiter=',')
    for row in csv_read:
        values.append(int(row['value']))
        timestamp.append(
                time.strptime(row['created_at'], "%Y-%m-%d %H:%M:%S UTC")
                )


# st_ix = value_index(timestamp)
st_ix1 = value_index(timestamp, 23, 0)
end_ix1 = value_index(timestamp, 24, 0)
st_ix2 = value_index(timestamp, 29, 0)
end_ix2 = value_index(timestamp, 30, 0)
sum2 = 0
# base1 = int(time.mktime(timestamp[end_ix2])) - int(time.mktime(timestamp[st_ix2]))
base2 = int(time.mktime(timestamp[end_ix2])) - int(time.mktime(timestamp[st_ix2]))
ts = 0
time_in_sec = []
for i in range(st_ix2, end_ix2):
    ts = int(time.mktime(timestamp[i+1])) - int(time.mktime(timestamp[i]))
    sum2 += values[i]*ts/100
    time_in_sec.append(
        int(time.mktime(timestamp[i]) - time.mktime(timestamp[st_ix2]))
        )


print("%.3f" % (sum2/base2))
print(base2)

plt.plot(time_in_sec, values[st_ix2:end_ix2])
# plt.plot(values[st_ix1:end_ix1])
plt.xticks(list(range(0, 86398, 10800)), list(range(0, 24, 3)))
plt.show()
