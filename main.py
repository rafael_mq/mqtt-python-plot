import paho.mqtt.client as mqtt
import time
import csv


# The callback for when the client receives a CONNACK response from the server.
AIO_USERNAME = "RafaelMQ"
AIO_KEY = "f6ed222e222c446088110578b7bd0146"
pwm_topic = "/feeds/pwmout"
def on_connect(client, userdata, flags, rc):
    # O subscribe fica no on_connect pois, caso perca a conexão ele a renova
    # Lembrando que quando usado o #, você está falando que tudo que chegar após a barra do topico, será recebido
    client.subscribe(AIO_USERNAME + pwm_topic, qos=1)
    print("Connected to \"/feeds/pwmout\"")

def on_message(client, userdata, msg):
    print(msg.topic + " - " + str(int(msg.payload)))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
print("Init")


client.username_pw_set(username=AIO_USERNAME, password=AIO_KEY)

client.connect("io.adafruit.com", port=1883, keepalive=60)

client.loop_start()
